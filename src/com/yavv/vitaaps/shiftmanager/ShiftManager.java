/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this tEmplate file, choose Tools | TEmplates
 * and open the tEmplate in the editor.
 */
package com.yavv.vitaaps.shiftmanager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.URL;
import javax.swing.border.*;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;
import java.util.List;
import javax.swing.Timer;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.time.temporal.ChronoUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

class Employee implements Serializable {

    LocalDate date; // Employee properties
    String name;
    double hrs;

    protected List<Employee> employees, temporaryEmpl;
    protected List<String> latestShifts;

    Employee(String name, LocalDate date, int hrs) {

        this.name = name;
        this.hrs = hrs;
        this.date = date;
        temporaryEmpl = new ArrayList<Employee>();
        employees = new ArrayList<Employee>();
        latestShifts = new ArrayList<>();

    }

    public void setWorkHrs(double hrs) {
        this.hrs = hrs;
    }

    public double getWorkHrs() {
        return hrs;
    }

    public void setEmployeeName(String name) {
        this.name = name;
    }

    public String getEmployeeName() {
        return name;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDate getDate() {
        return date;
    }

    @Override
    public String toString() {
        return ("Служител : " + this.getEmployeeName() + " ;" + "\n"
                + "" + " Оставащи работни часа : " + this.getWorkHrs());
    }

    public void SaveData() {
        URL savePath = getClass().getClassLoader().getResource("com/yavv/vitaaps/shiftmanager/resources/data.tmp");

        try {
            FileOutputStream fos = new FileOutputStream(savePath.getPath());

            try (ObjectOutputStream oos = new ObjectOutputStream(fos)) {
                oos.writeObject(employees);
                oos.writeObject(temporaryEmpl);
                oos.writeObject(latestShifts);
            }
        } catch (IOException ex) {
            System.err.println(ex);
        }
    }

    public void LoadData() {
        URL loadPath = getClass().getClassLoader().getResource("com/yavv/vitaaps/shiftmanager/resources/data.tmp");

        try {
            FileInputStream fis = new FileInputStream(loadPath.getPath());

            try (ObjectInputStream ois = new ObjectInputStream(fis)) {
                employees = (ArrayList<Employee>) ois.readObject();
                temporaryEmpl = (ArrayList<Employee>) ois.readObject();
                latestShifts = (ArrayList<String>) ois.readObject();
            }
        } catch (ClassNotFoundException | IOException e) {
            System.err.println(e);
        }
    }
}

class OnOff extends Employee {

    long startShift;
    double average, sh;
    String time;
    Duration elapsedTime; // Shift duration in (Duration).
    JFrame past = new JFrame();
    JPanel onoff = new JPanel(),
            pastShiftsHRS = new JPanel();

    URL resourceOn = getClass().getClassLoader().getResource("com/yavv/vitaaps/shiftmanager/resources/on.gif"), // Relative path
            resourceOff = getClass().getClassLoader().getResource("com/yavv/vitaaps/shiftmanager/resources/off.png");

    ImageIcon onIcon = new ImageIcon(resourceOn),
            offIcon = new ImageIcon(resourceOff);

    JLabel onAnim = new JLabel(onIcon, JLabel.CENTER),
            offPic = new JLabel(offIcon, JLabel.CENTER);
    JToggleButton shiftON = new JToggleButton("Начало"),
            shiftOFF = new JToggleButton("Край");
    ButtonGroup bgONOFF = new ButtonGroup();
    List<Double> records;

    OnOff(String name, LocalDate date, int hrs) {
        super(name, date, hrs);
        records = new ArrayList<>();
        bgONOFF.add(shiftON);
        bgONOFF.add(shiftOFF);
        onoff.setBorder(new TitledBorder("Текуща смяна :"));
        onoff.add(onAnim);
        onoff.add(offPic);
        offPic.setVisible(true);
        onAnim.setPreferredSize(new Dimension(20, 20));
        onAnim.setVisible(false);
        onoff.add(shiftON);
        onoff.add(shiftOFF);
        past.setSize(200, 100);
        past.add(pastShiftsHRS);
        past.setResizable(false);

        shiftON.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startShift = System.currentTimeMillis();
                offPic.setVisible(false);
                onAnim.setVisible(true);
                onoff.repaint();
                if (employees.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "Няма назначен екип/смени!", "Грешка!", JOptionPane.ERROR_MESSAGE);
                    shiftOFF.doClick(); // Recording the shift's length int hours if user forgets to stop it before exit.
                }
            }
        });
        shiftOFF.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                elapsedTime = Duration.ofMillis(System.currentTimeMillis() - startShift); // (Duration) of current shift.
                long endhoursLong = elapsedTime.toHours();// Duration in (long);
                double timePassed = Math.toIntExact(endhoursLong);

                String time = String.format("%02d:%02d",
                        elapsedTime.toHours(),
                        elapsedTime.toMinutes());
                //////
                onAnim.setVisible(false); // Shift in progress animation.
                offPic.setVisible(true);
                onoff.repaint(); // Repaint panel for animation.

                for (Employee Em : employees) {
                    if (Em.getDate().equals(LocalDate.now())) { // If employee is working today.
                        double val = Em.getWorkHrs(); // Employee's current work hours.
                        Em.setWorkHrs(val - timePassed); //
                    }
                }
                records.add(timePassed);
                latestShifts.add(time);
                shiftsAverage();
            }
        });
        if (latestShifts.size() > 10) { // Average hours if there are no recorded shifts.
            latestShifts.clear();
        }
        if (records.size() > 7) { // Clear list if more than 3 recorded shifts.
            records.clear();
        }
    }

    double shiftsAverage() {          // Average work hours of the past 7 shifs appended to
        records.stream() // suspended Employees.
                .mapToDouble(Double::doubleValue)
                .average()
                .orElse(0);
        return average;
    }

}

class Shifts extends OnOff {

    JButton GenerateShifts = new JButton("Изчисли!");
    JPanel genPnl = new JPanel(),
            TxPnl = new JPanel();
    JLabel persons = new JLabel("Сл-л/и");
    JTextPane txtPane;
    JCheckBox oneweek = new JCheckBox("1 с-ца", true),
            twoweeks = new JCheckBox("2 с-ци");
    ButtonGroup weekOrtwo = new ButtonGroup();

    JSpinner workersNum = new JSpinner(new SpinnerNumberModel(02, 01, 04, 1));
    int wiks, weeks, x, menCount, dayOfweek, returnWork;
    List<LocalDate> totalDates;
    DateTimeFormatter formatter;
    StringBuilder str;
    Locale bgDays;
    Period period;

    Shifts(String name, LocalDate date, int hrs) {
        super(name, date, hrs);
        bgDays = new Locale("bg", "BG"); // Bulgarian weekdays for table 

        txtPane = new JTextPane();
        txtPane.setContentType("text/html");
        txtPane.setEditable(false);

        str = new StringBuilder("<table>");
        formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy"); // Footer time label
        genPnl.setBorder(new TitledBorder("Изчисли смени за :"));
        wiks = 7;
        totalDates = new ArrayList<LocalDate>();
        weekOrtwo.add(oneweek);
        weekOrtwo.add(twoweeks);
        genPnl.add(workersNum);
        genPnl.add(persons);
        genPnl.add(oneweek);
        genPnl.add(twoweeks);
        genPnl.add(GenerateShifts);

        TxPnl.setLayout(new BorderLayout());
        TxPnl.setBorder(new TitledBorder("Готови смени : "));
        TxPnl.setPreferredSize(new Dimension(550, 460));
        TxPnl.add(new JScrollPane(txtPane), BorderLayout.CENTER);

        oneweek.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                wiks = 7;
            }
        });
        twoweeks.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                wiks = 14;
            }
        });
        GenerateShifts.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (employees.isEmpty()) {
                    txtPane.setText("<html><center>Няма Екип!</center></html>");
                } else {
                    str.setLength(0);
                    nextWorkday(); // Fills the dates array with the following 2 weeks
                    backtoWork(); // Checks for returning workers.
                    menCount = (Integer) workersNum.getValue(); // Men in a shift equals spinner value.

                    for (int i = 0; i < wiks; i++) { // One or two weeks shedule from JCheckboxes.

                        employees.sort(Comparator.comparing(Employee::getWorkHrs).reversed().thenComparing(Employee::getEmployeeName));
                        int count = 0;
                        int dateNum = i;

                        if (menCount == 1) {

                            str.append("<table border=\"1\" align=center>"
                                    + "<tr><th style=\"width:70px\"> - </th>"
                                    + "<th style=\"width:90px\">"
                                    + totalDates.get(dateNum).getDayOfWeek().getDisplayName(TextStyle.FULL, bgDays).toString() + "</th></tr>");
                            // Days of the week in Bulgarian using new Local.
                        }
                        if (menCount == 2) {
                            if (employees.size() < 2) { // Total number of Employees on team, check. Can't be lower than requiered for shift.
                                JOptionPane.showMessageDialog(TxPnl, "Недостатъчен брой служители!", "Грешка!", 2);
                                break;
                            } else {
                                str.append("<table border=\"1\" align=center>"
                                        + "<tr><th style=\"width:70px\"> # </th><th style=\"width:70px\"> # </th>"
                                        + "<th style=\"width:90px\">"
                                        + totalDates.get(dateNum).getDayOfWeek().getDisplayName(TextStyle.FULL, bgDays).toString() + "</th></tr>");
                            }
                        }
                        if (menCount == 3) {
                            if (employees.size() < 3) {
                                JOptionPane.showMessageDialog(TxPnl, "Недостатъчен брой служители за смяната!", "Грешка!", 2);
                                break;
                            } else {
                                str.append("<table border=\"1\" align=center>"
                                        + "<tr><th style=\"width:70px\"> # </th><th style=\"width:70px\"> # </th>"
                                        + "<th style=\"width:70px\"> # </th>"
                                        + "<th style=\"width:90px\">"
                                        + totalDates.get(dateNum).getDayOfWeek().getDisplayName(TextStyle.FULL, bgDays).toString() + "</th></tr>");
                            }
                        }
                        if (menCount == 4) {
                            if (employees.size() < 4) {
                                JOptionPane.showMessageDialog(TxPnl, "Недостатъчен брой служители за смяната!", "Грешка!", 2);
                                break;
                            } else {
                                str.append("<table border=\"1\" align=center>"
                                        + "<tr><th style=\"width:70px\"> # </th><th style=\"width:70px\"> # </th>"
                                        + "<th style=\"width:70px\"> # </th><th style=\"width:70px\"> # </th>"
                                        + "<th style=\"width:90px\">" + totalDates.get(dateNum).getDayOfWeek().getDisplayName(TextStyle.FULL, bgDays).toString() + "</th></tr>");
                            }
                        }
                        for (Employee Em : employees) {

                            Em.setDate(totalDates.get(dateNum)); // Every worker gets a shift date to their object.
                            //int val = Em.getWorkHrs(); // Reducing of a worker's total hours to work. // TESTING
                            //Em.setWorkHrs(val - average);    // // Average of the LAST 3 SHIFTs as a BASE // TESTING

                            str.append("<td align=center>" + Em.getEmployeeName() + "</td> \n");

                            count++; // Counter for iterations over REORDERED list with workers with most work hours.
                            x = i;
                            if (count == menCount) { // Men in a shift.
                                int daysCount = x;

                                str.append("<td align=center>" + totalDates.get(daysCount).format(formatter) + "</td></tr>  \n"
                                        + "</table> ");

                                break;
                            }
                        }
                    }
                    txtPane.setText(str.toString());
                }
            }
        });
    }

    void nextWorkday() { // Array with the next 2 weeks from current date.
        LocalDate start = LocalDate.now().plusDays(1);
        LocalDate end = LocalDate.now().plusDays(14);
        while (!start.isAfter(end)) {
            totalDates.add(start);
            start = start.plusDays(1);
        }
    }

    public void backtoWork() { // Temporary removed Employees are added back to main team automatically.
        for (Employee Em : temporaryEmpl) {
            period = totalDates.get(x).until(Em.getDate());
            returnWork = period.getDays();
            if (returnWork == 1) {
                employees.add(Em);
                temporaryEmpl.remove(Em);
            }
        }
    }
}

class Temporary extends Shifts { // JComboBox

    JFrame sickframe = new JFrame(),
            backframe = new JFrame();
    JPanel sickPnl = new JPanel(),
            backPnl = new JPanel();
    JComboBox cb, cb2;
    JButton applyRmTemp = new JButton("Добави дните!"),
            back = new JButton("Върни в Екип-а");
    JSpinner absence;

    Temporary(String name, LocalDate date, int hrs) {
        super(name, date, hrs);
        cb = new JComboBox();
        cb2 = new JComboBox();
        absence = new JSpinner(new SpinnerNumberModel(00, 00, 31, 1));
        sickframe.setSize(500, 120);
        sickframe.setResizable(false);
        sickframe.setLocationRelativeTo(TxPnl);
        backframe.setSize(500, 120);
        backframe.setResizable(false);
        backframe.setLocationRelativeTo(TxPnl);
        backPnl.add(cb2);
        backPnl.add(back);
        backPnl.setLayout(new FlowLayout());
        backPnl.setBorder(new TitledBorder("Върни в Екип-а"));
        sickPnl.add(cb);
        sickPnl.add(absence);
        sickPnl.add(applyRmTemp);
        sickPnl.setLayout(new FlowLayout());
        sickPnl.setBorder(new TitledBorder("Отпуска/Болнични дни :"));
        sickframe.add(sickPnl);
        backframe.add(backPnl);

    }
}

class MenuBar extends Temporary {

    JMenuBar mb = new JMenuBar();
    JMenu file = new JMenu("Файл"),
            change = new JMenu("/Екип/"),
            pastshifts = new JMenu("Последни смени");
    LocalDate d1, d2;
    FileReader reader;

    JMenuItem print = new JMenuItem("Печат"),
            exit = new JMenuItem("Изход"),
            add = new JMenuItem("Добави служител"),
            remove = new JMenuItem("Премахни служител"),
            removeAllreg = new JMenuItem("Премахни Екип!"),
            removeTemp = new JMenuItem("Извади временно"),
            backTemp = new JMenuItem("Върни служител по-рано"),
            viewTemp = new JMenuItem("Списък временно извадени"),
            setallhours = new JMenuItem("Стартови часове за Екип"),
            pastShifts = new JMenuItem("Последни смени - часове"),
            view = new JMenuItem("Текущ Екип"),
            about = new JMenuItem("Помощ");

    MenuBar(String name, LocalDate date, int hrs) {
        super(name, date, hrs);
        file.add(print);
        file.add(about);
        file.add(new JSeparator());
        file.add(exit);
        change.add(view);
        change.add(new JSeparator());
        change.add(add);
        change.add(remove);
        change.add(removeAllreg);
        change.add(new JSeparator());
        change.add(setallhours);
        change.add(new JSeparator());
        change.add(removeTemp);
        change.add(backTemp);
        change.add(viewTemp);
        pastshifts.add(pastShifts);

        mb.setBorder(new EtchedBorder());
        mb.add(file);
        mb.add(change);
        mb.add(pastshifts);

        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ex) {
                if (onAnim.isVisible()) { // Record current active shift on exit,
                    shiftOFF.doClick();     // if recording animation is active.
                }
                SaveData();
                System.exit(0);
            }
        });
        about.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    URL helpFile = getClass().getClassLoader().getResource("com/yavv/vitaaps/shiftmanager/resources/SM.txt");
                    reader = new FileReader(helpFile.getPath());
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(MenuBar.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    txtPane.read(reader, "SM.txt");
                    StyledDocument doc = txtPane.getStyledDocument();
                    SimpleAttributeSet style = new SimpleAttributeSet();
                    StyleConstants.setLeftIndent(style, 10);
                    doc.setParagraphAttributes(0, doc.getLength(), style, true);
                } catch (IOException ex) {
                    Logger.getLogger(MenuBar.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String input = (String) JOptionPane.showInputDialog(TxPnl, "Въведете нов член на Екипа :", "Нов служител", JOptionPane.QUESTION_MESSAGE);
                int startingHrs = 36;
                if (input != null) {
                    if (!input.matches("^[а-яА-Я]+(?:\\s[а-яА-Я]+)*$")) {
                        JOptionPane.showMessageDialog(TxPnl, "Грешка! Кирилица + първо име!", "Грешка!", JOptionPane.ERROR_MESSAGE);
                    } else {
                        Employee newGuy = new Employee(input, LocalDate.now(), startingHrs);
                        employees.add(newGuy);
                        JOptionPane.showMessageDialog(TxPnl, "Името е добавено е Екипа!", "Съобщение", JOptionPane.PLAIN_MESSAGE);
                        cb.setModel(new DefaultComboBoxModel(employees.toArray()));
                    }
                } else {
                    JOptionPane.showMessageDialog(TxPnl, "Въвеждането преустановено!", "Грешка!", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        remove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String EmplDel = (String) JOptionPane.showInputDialog(TxPnl, "Име :", "Изтрийте член на Екипа", JOptionPane.QUESTION_MESSAGE);
                boolean nameExist = employees.stream().anyMatch(Employee -> EmplDel.equals(Employee.getEmployeeName())); // Name check
                if (EmplDel != null) {
                    if (nameExist != true) {
                        JOptionPane.showMessageDialog(TxPnl, "Няма такова име/натиснахте CANCEL", "Съобщение/Грешка", JOptionPane.ERROR_MESSAGE);
                    } else {
                        employees.removeIf(Employee -> Employee.getEmployeeName().contains(EmplDel));
                        JOptionPane.showMessageDialog(TxPnl, "Името е премахнато от Екипа!", "Съобщение", JOptionPane.PLAIN_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(TxPnl, "Няма такова име/натиснахте CANCEL", "Съобщение/Грешка", JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        removeTemp.addActionListener(new ActionListener() { // Remove worker temporarily.
            @Override
            public void actionPerformed(ActionEvent e) {
                sickframe.setVisible(true);
                sickframe.setLocationRelativeTo(TxPnl);
                cb.setModel(new DefaultComboBoxModel(employees.toArray()));
                if (employees.isEmpty()) {
                    cb.addItem("Екипът е празен!");
                    applyRmTemp.setVisible(false);
                } else {
                    applyRmTemp.setVisible(true);
                }
            }
        });
        backTemp.addActionListener(new ActionListener() { // Get worker back earlier to work
            @Override
            public void actionPerformed(ActionEvent e) {
                backframe.setVisible(true);
                backframe.setLocationRelativeTo(TxPnl);
                cb2.setModel(new DefaultComboBoxModel(temporaryEmpl.toArray()));
                if (temporaryEmpl.isEmpty()) {
                    cb2.addItem("Екипът е празен!");
                    back.setVisible(false);
                } else {
                    back.setVisible(true);
                }
            }
        });
        removeAllreg.addActionListener(new ActionListener() { // Remove all starting base work hours.
            @Override
            public void actionPerformed(ActionEvent e) {
                int dialogResult = JOptionPane.showConfirmDialog(TxPnl, "Сигурни ли сте, че искате да изтриете Екипа?", "Внимание!!!", JOptionPane.YES_NO_OPTION);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    employees.clear();
                    JOptionPane.showMessageDialog(TxPnl, "Екипът е изтрит", "Съобщение", JOptionPane.PLAIN_MESSAGE);
                } else {

                }

            }
        });
        applyRmTemp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double daysWorkHrs = (double) absence.getValue() * average; // dates of absence in work hours times average workhours for last 3 shifts.
                double dateBack = (double) absence.getValue(); // number of days of absence.
                int datebackk = (int) dateBack;
                int listitem = cb.getSelectedIndex();
                d1 = LocalDate.now();//// Date of leaving.
                employees.get(listitem).setDate(LocalDate.now().plusDays(datebackk)); // Date of return.
                double currHrs = employees.get(listitem).getWorkHrs();
                employees.get(listitem).setWorkHrs(currHrs + daysWorkHrs);
                temporaryEmpl.add(employees.get(listitem)); // Adding to temporary removed Employees array

                employees.remove(employees.get(listitem)); // Removing from active work team.
                sickframe.dispose();
                absence.setValue(0);
                JOptionPane.showMessageDialog(TxPnl, "Дните са добавени! Служителят е отстранен временно!", "Временно отстранени", JOptionPane.PLAIN_MESSAGE);
            }
        });
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int listitem = cb2.getSelectedIndex();
                d2 = LocalDate.now();

                double compensate = ChronoUnit.DAYS.between(d1, d2) * average;
                double backnow = temporaryEmpl.get(listitem).getWorkHrs();

                temporaryEmpl.get(listitem).setWorkHrs(backnow - compensate);
                temporaryEmpl.get(listitem).setDate(d2);

                employees.add(temporaryEmpl.get(listitem));
                temporaryEmpl.remove(temporaryEmpl.get(listitem));
                backframe.dispose();
                JOptionPane.showMessageDialog(TxPnl, "Служителят е върнат в Екип-а!", "Временно отстранени", JOptionPane.PLAIN_MESSAGE);
            }
        });

        view.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent view) {
                if (employees.isEmpty()) {
                    JOptionPane.showMessageDialog(TxPnl, "Екипът е празен!", "Текущ Екип", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    employees.sort(Comparator.comparing(Employee::getWorkHrs).reversed());
                    String listString = employees.stream().map(Object::toString).collect(Collectors.joining(",\n"));
                    JOptionPane.showMessageDialog(TxPnl, listString, "Текущ Екип", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        viewTemp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent view) {
                if (temporaryEmpl.isEmpty()) {
                    JOptionPane.showMessageDialog(TxPnl, "Няма отстранени служители!", "Текущ Екип", JOptionPane.INFORMATION_MESSAGE);
                } else {
                    temporaryEmpl.sort(Comparator.comparing(Employee::getWorkHrs).reversed());
                    String listString = temporaryEmpl.stream().map(Object::toString).collect(Collectors.joining(",\n"));
                    JOptionPane.showMessageDialog(TxPnl, listString, "Временно отстранени", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        setallhours.addActionListener((new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent setall) {
                try {
                    int inputHRS = Integer.parseInt(JOptionPane.showInputDialog(TxPnl, "Оставащи часове за всеки работник:",
                            "Въведете нови стартови часове!", 3));
                    for (Employee em : employees) {
                        em.setWorkHrs(inputHRS);
                    }
                    JOptionPane.showMessageDialog(TxPnl, "Базовите часове за всички служители са вече " + inputHRS, "Базови часове", JOptionPane.PLAIN_MESSAGE);
                } catch (NumberFormatException ex) {
                    System.err.println(ex);
                }
            }
        }));
        pastShifts.addActionListener(new ActionListener() { // Show array of all past shifts lengths.
            @Override
            public void actionPerformed(ActionEvent e) {
                if (latestShifts.isEmpty()) {
                    JOptionPane.showMessageDialog(TxPnl, "Няма записани минали смени!", "Грешка!", JOptionPane.ERROR_MESSAGE);
                } else {
                    String latest = latestShifts.stream().map(Object::toString).collect(Collectors.joining(",\n"));
                    JOptionPane.showMessageDialog(TxPnl, latest, "Последни смени", JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
        print.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    boolean complete = txtPane.print();
                    if (complete) {
                        JOptionPane.showMessageDialog(TxPnl, "Принтирането е завършено!", "Принтиране", JOptionPane.INFORMATION_MESSAGE);
                    }
                } catch (PrinterException ex) {
                    JOptionPane.showMessageDialog(TxPnl, "Принтирането неуспешно!", "Принтиране", JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(MenuBar.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
    }
}

public class ShiftManager extends MenuBar {

    private JFrame face = new JFrame("Мениджър смени");

    private final URL resource = getClass().getClassLoader().getResource("com/yavv/vitaaps/shiftmanager/resources/vita.png");
    private final ImageIcon vita = new ImageIcon(resource);

    private final JLabel clkLabel = new JLabel(),
            dateLabel = new JLabel(LocalDate.now().format(DateTimeFormatter.ofPattern("dd/MM/yy"))),
            companyLogo = new JLabel(vita, JLabel.CENTER);
    final JLabel companyName = new JLabel("<html><font size=5>VitaGrain BG АД</font></html>");
    final JLabel author = new JLabel("<html><i><font size=2>YavkU™</font></i></html>");
    private final JPanel clkPanl = new JPanel(new BorderLayout()),
            onoffGenPnl = new JPanel(),
            company = new JPanel(new FlowLayout()),
            clockDateCompany = new JPanel(new BorderLayout());
    private final static int ONE_SECOND = 1000;
    private final SimpleDateFormat clockFormat = new SimpleDateFormat("HH:mm:ss");
    private final Border border, margin;

    ShiftManager(String name, LocalDate date, int hrs) {

        super(name, date, hrs);

        clkLabel.setFont(new Font(clkLabel.getFont().getName(), Font.PLAIN, 12));
        dateLabel.setFont(new Font(clkLabel.getFont().getName(), Font.PLAIN, 12));
        clkPanl.add(clkLabel, BorderLayout.NORTH);
        clkPanl.add(dateLabel, BorderLayout.CENTER);
        clkPanl.add(author, BorderLayout.SOUTH);
        companyLogo.setPreferredSize(new Dimension(30, 33));
        company.add(companyLogo);
        company.add(companyName);

        border = clockDateCompany.getBorder();
        margin = new EmptyBorder(5, 0, 0, 5);
        clockDateCompany.setBorder(new CompoundBorder(border, margin)); // Footer border
        clockDateCompany.add(company, BorderLayout.WEST);
        clockDateCompany.add(clkPanl, BorderLayout.EAST);

        face.setBounds(600, 200, 600, 440);
        face.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        face.setLayout(new BorderLayout());
        face.setResizable(false);
        onoffGenPnl.setLayout(new BorderLayout());
        onoffGenPnl.add(mb, BorderLayout.NORTH);
        onoffGenPnl.add(onoff, BorderLayout.WEST);
        onoffGenPnl.add(genPnl, BorderLayout.CENTER);

        face.add(onoffGenPnl, BorderLayout.NORTH);
        face.add(TxPnl, BorderLayout.CENTER);
        face.add(clockDateCompany, BorderLayout.SOUTH);
        face.pack();
        face.setVisible(true);
        LoadData();

        face.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (onAnim.isVisible()) {                                       // Save current workshift data on exit
                    shiftOFF.doClick();                                         // if shift recorder is active
                }
                SaveData();
                face.dispose();
                System.exit(0);
            }
        });
        if (employees.isEmpty()) { // Checks for active team of Employees and disables functions if none.
            GenerateShifts.setEnabled(false);
            shiftON.setEnabled(false);
            txtPane.setText("<html><center>Няма Екип! Рестартирайте след промяна.</center></html>");
        }
    }

    public void clockey() {

        Timer timer = new Timer(ONE_SECOND, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clkLabel.setText(clockFormat.format(new Date()));
                clkLabel.repaint();
            }
        });
        clkLabel.setText(clockFormat.format(new Date()));
        timer.start();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            try {
                ShiftManager shftMan = new ShiftManager(null, null, 0);
                shftMan.clockey();
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Фатална грешка! ShiftManager не може да стартира!", "Грешка!", JOptionPane.ERROR_MESSAGE);
            }
        });

    }
}
